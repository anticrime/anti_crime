<?php 

	include 'db_conn.php';

	if (isset($_POST['submit'])) {
		
		$job_title = mysqli_real_escape_string($db, $_POST['job_title']);
		$phone_number = mysqli_real_escape_string($db, $_POST['phone_number']);
		$age = mysqli_real_escape_string($db, $_POST['age']);
		$e_id = mysqli_real_escape_string($db, $_POST['e_id']);
		
		$file = $_FILES['file'];
		if (empty($job_title) || empty($phone_number) || empty($age) || empty($e_id)) {
			echo "<script>alert('please fill all fields')</script>";
			echo "<script>window.open('../jobs.php', '_self')</script>";
			exit();
		}else{
			//check if he has already applied before job
			$sql = "SELECT e_id FROM job_applicants WHERE e_id='$e_id'";
			$result = mysqli_query($db, $sql);
			$checkResults = mysqli_num_rows($result);
			if ($checkResults > 0) {				
				echo "<script>alert('You have already applied for a job!!')</script>";
				echo "<script>window.open('../jobs.php', '_self')</script>";
				exit();
			}else{
				//insert user application details
				move_uploaded_file($file['tmp_name'], "../uploads/jobApplications/". $file['name']);

				$query = "INSERT INTO job_applicants(job_title,phone_number,age,e_id) VALUES('$job_title','$phone_number','$age','$e_id')";
				mysqli_query($db, $query);				
				echo "<script>alert('application submitted successfully')</script>";
				echo "<script>window.open('../jobs.php', '_self')</script>";
				exit();
			}
		}
	}

 ?>