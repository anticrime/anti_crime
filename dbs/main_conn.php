<?php 
	
	include 'db_conn.php';

	if (isset($_POST['submit'])) {
		
		$first_name = mysqli_real_escape_string($db, $_POST['first_name']);
		$last_name = mysqli_real_escape_string($db, $_POST['last_name']);
		$email = mysqli_real_escape_string($db, $_POST['email']);
		$national_id = mysqli_real_escape_string($db, $_POST['national_id']);
		$county = mysqli_real_escape_string($db, $_POST['county']);
		$edu_level = mysqli_real_escape_string($db, $_POST['edu_level']);
		$username = mysqli_real_escape_string($db, $_POST['username']);
		$pass1 = mysqli_real_escape_string($db, $_POST['pass1']);
		$pass2 = mysqli_real_escape_string($db, $_POST['pass2']);

		if (empty($first_name) || empty($last_name) || empty($email) || empty($national_id) || empty($county) || empty($edu_level) || empty($username) ||  empty($pass1) || empty($pass2)) {
			header("Location: ../signup.php?some fields are empty!!");
			exit();
		}else{
			//check if national id is taken
			$sql = "SELECT national_id FROM employees WHERE national_id='$national_id'";
			$result = mysqli_query($db, $sql);
			$checkResult = mysqli_num_rows($result);
			if ($checkResult > 0) {
				header("Location: ../signup.php?Id is taken!!");
				exit();
			}else{
				//check if pass match
				if ($pass2 !== $pass1) {
					header("Location: ../signup.php?Passwords dont match!!");
					exit();
				}else{
					//check if username exists
					$u_name = "SELECT username FROM employees WHERE username='$username'";
					$record = mysqli_query($db, $u_name);
					if (mysqli_num_rows($record) > 0) {
						header("Location: ../signup.php?Username has been taken!!");
						exit();
					}else{
						//hash the password
						$pwd = sha1($pass1);

						//insert details to dbs
						$query = "INSERT INTO employees(first_name,last_name,email,national_id,county,edu_level,username,password) VALUES('$first_name','$last_name','$email','$national_id','$county','$edu_level','$username','$pwd')";
						mysqli_query($db,$query);
						header("Location: ../login.php?signup=success!!");
						exit();
					}
					
				}
			}
		}
	}

	if (isset($_POST['login'])) {
		$username = mysqli_real_escape_string($db, $_POST['username']);
		$pwd = mysqli_real_escape_string($db, $_POST['pwd']);

		if (empty($username) || empty($pwd)) {
			header("Location: ../login.php?some fields are empty!!");
			exit();
		}else{
			$pass = sha1($pwd);
			$sql = "SELECT * FROM employees WHERE username='$username' AND password='$pass'";
			$results = mysqli_query($db, $sql);
			$row = mysqli_fetch_assoc($results);
			if (mysqli_num_rows($results) == 1) {
				$_SESSION['e_id'] = $row['employee_id'];
				$_SESSION['f_name'] = $row['first_name'];
				$_SESSION['l_name'] = $row['last_name'];
				$_SESSION['email'] = $row['email'];
				$_SESSION['n_id'] = $row['national_id'];
				$_SESSION['county'] = $row['county'];
				$_SESSION['message'] = 'You are now logged in!!';
				header("Location: ../homepage.php?login=success!!");
				exit();
			}else{
				header("Location: ../login.php?Wrong details!!");
				exit();
			}
		}
	}

	if (isset($_POST['logout'])) {
		session_start();
		session_unset();
		session_destroy();
		header("Location: ../login.php");
		exit();
	}

 ?>