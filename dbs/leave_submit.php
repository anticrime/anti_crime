<?php 
	
	include 'db_conn.php';

	if (isset($_POST['submit_leave'])) {
		
		$leave_reason = mysqli_real_escape_string($db, $_POST['leave_reason']);
		$start_date = mysqli_real_escape_string($db, $_POST['start_date']);
		$end_date = mysqli_real_escape_string($db, $_POST['end_date']);
		$e_id = mysqli_real_escape_string($db, $_POST['e_id']);

		if (empty($leave_reason) || empty($start_date) || empty($end_date)) {
			echo "<script>alert('please fill all fields')</script>";
			echo "<script>window.open('../leave.php', '_self')</script>";			
			exit();
		}else{
			//check if the user exists on off records
			$query = "SELECT e_id FROM on_leave WHERE e_id='$e_id'";
			$result = mysqli_query($db, $query);
			$checkResult = mysqli_num_rows($result);
			if ($checkResult > 0) {
				echo "<script>alert('you have already applied for an off, it's processing)</script>";
				echo "<script>window.open('../leave.php', '_self')</script>";			
				exit();
			}else{
				$sql = "INSERT INTO on_leave(leave_reason,start_date,end_date,e_id) 
				VALUES('$leave_reason','$start_date','$end_date','$e_id')";
				mysqli_query($db, $sql);
				echo "<script>alert('application for leave is successfull')</script>";
				echo "<script>window.open('../leave.php', '_self')</script>";			
				exit();
			}
			
		}
	}

 ?>